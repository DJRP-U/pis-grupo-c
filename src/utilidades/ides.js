export const saveID = (id, uno, marca) => {
    localStorage.setItem('id', id);
    localStorage.setItem('uno', uno);
    localStorage.setItem('marca', marca);
};

export const getID = () => {
    const id = localStorage.getItem('id');
    const uno = localStorage.getItem('uno');
    return { id, uno };
};


export const eliminarID = () => {
    localStorage.removeItem('id');
    localStorage.removeItem('uno');
};

export const saveIdenti = (id) => {
    localStorage.setItem('identificacion', id);
};

export const getIdenti = () => {
    const id = localStorage.getItem('identificacion');
    return id;
};
export const eliminarIdenti = () => {
    localStorage.removeItem('identificacion');
};

//Materia
export const saveMateria = (external_materia) => {
  localStorage.setItem('external_materia', external_materia);
};

export const getMateria = () => {
  const id = localStorage.getItem('external_materia');
  return id;
};

export const elimianrTodo = () => {
    localStorage.clear();
};

export const eliminarRepuesto = () => {
    localStorage.removeItem('repuesto');
};

export const saveRepuesto = (repuestos) => {
        localStorage.setItem('repuesto', JSON.stringify(repuestos));
  };
  
  export const getRepuesto = () => {
    const repuestos = localStorage.getItem('repuesto');
  //  return JSON.parse(repuestos) ;
    return repuestos ;
  };

  export const eliminarCodigo = () => {
    localStorage.removeItem('codigo');
};

export const saveCodigo = (repuestos) => {
        localStorage.setItem('codigo', repuestos);
  };
  
  export const getCodigo = () => {
    const repuestos = localStorage.getItem('codigo');
  //  return JSON.parse(repuestos) ;
    return repuestos ;
  };
  export const eliminarExPract= () => {
    localStorage.removeItem('exterPract');
};

export const saveExPract= (repuestos) => {
        localStorage.setItem('exterPract', repuestos);
  };
  
  export const getExPract = () => {
    const repuestos = localStorage.getItem('exterPract');
  //  return JSON.parse(repuestos) ;
    return repuestos ;
  };
  export const eliminarTrabajo= () => {
    localStorage.removeItem('trabajo');
};

export const saveTrabajo= (repuestos) => {
        localStorage.setItem('trabajo', repuestos);
  };
  
  export const getTrabajo = () => {
    const repuestos = localStorage.getItem('trabajo');
  //  return JSON.parse(repuestos) ;
    return repuestos ;
  };
  export const eliminarConfir= () => {
    localStorage.removeItem('confir');
};

export const saveConfir= (repuestos) => {
        localStorage.setItem('confir', repuestos);
  };
  
  export const getConfir = () => {
    const repuestos = localStorage.getItem('confir');
  //  return JSON.parse(repuestos) ;
    return repuestos ;
  };
  







