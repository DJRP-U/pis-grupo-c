import './App.css';
import { Navigate, Route, Routes, useLocation } from 'react-router-dom';
import Login from './fragmnets/Login';
import Principal from './fragmnets/Principal';
import Persona, { PresentarPersona } from './fragmnets/PresentarPersona';
import Consola from './fragmnets/Consola'
import RegistroPersonas from './fragmnets/RegistrarPersona';
import { estaSesion, soloAdmin, soloProfesor } from './utilidades/Sessionutil';
import Registrar from './fragmnets/Registrar';
import Materia from './fragmnets/Materia';
import AgregarPracticas from './fragmnets/AgregarPracticas';
import PracticaEntregarr from './fragmnets/PracticaEntregarr';
import RegistrarPractica from './fragmnets/RegistrarPractica';
import PresentarPractica from './fragmnets/PresentarPractica';
import PresentarEntregables from './fragmnets/PresentarEntregables';
import PresentarPracticaEStudiante from './fragmnets/PresentarPracticaEStudiante';
import Sidebar from './fragmnets/Sidebar';
import Perfil from './fragmnets/Perfil';

function App() {
  const Middeware = ({ children }) => {
    const autenticado = estaSesion();
    const location = useLocation();
    if (autenticado) {
      return children;
    } else {
      return <Navigate to='/login' state={location} />;
    }
  }

  const MiddewareAdmin = ({ children }) => {
    const autenticado = soloAdmin();
    const location = useLocation();
    if (autenticado) {
      return children;
    } else {
      return <Navigate to='/principal' state={location} />;
    }
  }
  const MiddewareProfesor = ({ children }) => {
    const autenticado = soloProfesor();
    const location = useLocation();
    if (autenticado) {
      return children;
    } else {
      return <Navigate to='/principal' state={location} />;
    }
  }

  const MiddewareSesion = ({ children }) => {
    const autenticado = estaSesion();
    if (autenticado) {
      return <Navigate to='/principal' />;

    } else {
      return children;
    }
  }

  return (
    <div className="App">
      <Routes>
      <Route path='/' element={<MiddewareSesion><Login /></MiddewareSesion>} />
        <Route path='/login' element={<MiddewareSesion><Login /></MiddewareSesion>} />
        <Route path='/registrar' element={<MiddewareAdmin><Registrar /></MiddewareAdmin>} />
        <Route path='/principal' element={<Principal /> }/>
        <Route path='/practicadoc' element={<MiddewareProfesor><PresentarPractica/></MiddewareProfesor>} />
        <Route path='/practica/estudiante' element={<PresentarPracticaEStudiante/>} />
        <Route path='/practica/agregar' element={<MiddewareProfesor><AgregarPracticas/></MiddewareProfesor>}/>
        <Route path='/consola' element={<Consola />} />
        <Route path='/visualizar/practica' element={<PracticaEntregarr />} />
        <Route path='/calificar/practica' element={<MiddewareProfesor><PresentarEntregables /></MiddewareProfesor>} />
        <Route path='/matricular' element={<PresentarPersona />} />
        <Route path='/registrar' element={<Middeware><RegistroPersonas/></Middeware>} />
        <Route path='/persona/matricular' element={<Middeware><Persona/></Middeware>} />
        <Route path='/persona/' element={<Middeware><Persona/></Middeware>} />
        <Route path='/materia' element={<Middeware><Materia/></Middeware>}/>
        <Route path='/sidebar' element={<Middeware><Sidebar/></Middeware>}/>
        <Route path='/perfil' element={<Middeware><Perfil/></Middeware>}/>
        <Route path='/practica/registrar' element={<Middeware><RegistrarPractica /></Middeware>} />
      </Routes>
    </div>
  );
}


export default App;
