import '../css/style.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useEffect, useState } from 'react';
import { Badge, Button, ListGroup } from 'react-bootstrap';
import { Practicas } from '../hooks/Conexion';
import { borrarSesion, getToken } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensajes';
import { findAllByDisplayValue } from '@testing-library/react';

const Activities = () => {

    const [practicas, setPracticas] = useState([]);
    const [llPracticas, setllPracticas] = useState(false);

    if (!llPracticas){
        Practicas(getToken()).then((info) => {
                console.log(info);
            if (info.code !== 200) {
                borrarSesion();
                mensajes(info.msg);
                window.location.href = '/login';
    
            } else {
                setPracticas(info.info);
                console.log(practicas);
                setllPracticas(true);
            }
        });
    }
    


    const saveExternalIdLocally = (externalId) => {
        localStorage.setItem('externalId', externalId);
    };

    const redirectToConsole = () => {
        window.location.href = '/consola';
    };
    console.log()
    return (
        <ListGroup as="ol">
            {practicas.map((activity) => (
                <ListGroup.Item
                    key={activity.external_id}
                    as="li"
                    className="d-flex justify-content-between align-items-start"
                >
                    <Button variant="outline-primary" onClick={() => {
                        saveExternalIdLocally(activity.external_id);
                        redirectToConsole();
                    }}>
                        Acceder
                    </Button>{' '}
                    <div className="ms-2 me-auto">
                        <div className="fw-bold">{activity.titulo}</div>
                        {activity.descripcion}
                    </div>
                    <Badge
                        bg={activity.actividad === 'PENDIENTE' ? 'primary' : activity.actividad === 'ENTREGADO' ? 'danger' : 'success'}
                        pill
                    >
                        {activity.actividad}
                    </Badge>
                </ListGroup.Item>
            ))}
        </ListGroup>
    );
}
export default Activities;