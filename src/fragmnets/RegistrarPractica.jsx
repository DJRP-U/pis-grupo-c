import { useNavigate } from 'react-router';
import { GuardarPractica } from '../hooks/Conexion';
import { Form, Button } from 'react-bootstrap';
import { useForm } from 'react-hook-form';
import { useState } from 'react';
import { getToken } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensajes';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
export const RegistrarPractica = () => {
    const { register, handleSubmit, formState: { errors } } = useForm();
    const navegation = useNavigate();
    const [marcas, setMarcas] = useState([]);//para listar marcas
    const [llmarcas, setLlmarcas] = useState(false);//para listar marcas
    const { watch, setValue } = useForm();//para listar marcas

    //acciones
    // onsubmit
    const onSubmit = (data) => {
        var datos = {
            "titulo": data.titulo,
            "descripcion": data.descripcion,
            "actividad": data.actividad,
            "fechaInicio": data.fechaInicio,
            "fechaFin": data.fechaFin
        };
        GuardarPractica(datos, getToken()).then((info) => {
            console.log(datos);
            if (info.error === true) {
                mensajes(info.msg, 'error', 'Error');
            } else {
                mensajes(info.msg);
                navegation("/principal");
            }
        }
        );
    };



    return (
        <div className="wrapper">
            <div className="d-flex flex-column">
                <div className="content">

                    <div className='container-fluid'>
                        <div className="col-lg-10">
                            <div className="p-5">

                                <form className="user" onSubmit={handleSubmit(onSubmit)}>

                                    {/** ESCOGER TITULO */}
                                    <div className="form-group">
                                        <input type="text" {...register('titulo', { required: true })} className="form-control form-control-user" placeholder="Ingrese Un titulo" />
                                        {errors.titulo === 'required' && <div className='alert alert-danger'>Escriba los Nombres</div>}
                                    </div>

                                    {/** ESCOGER DESCRIPCION */}
                                    <div className="form-group">
                                        <input type="text" {...register('descripcion', { required: true })} className="form-control form-control-user" placeholder="Ingrese una descripcion" />
                                        {errors.descripcion === 'required' && <div className='alert alert-danger'>Escriba una descripcion</div>}
                                    </div>
                                    {/** ESCOGER ACTIVIDAD */}
                                    <div className="form-group">
                                        <Form.Control as="select" {...register('actividad', { required: true })}>
                                            <option value="">Seleccione una actividad</option>
                                            <option value="PENDIENTE">Pendiente</option>
                                            <option value="ENTREGADO">Entregado</option>
                                            <option value="REVISADO">Revisado</option>
                                            <option value="OCULTO">Oculto</option>
                                        </Form.Control>
                                        {errors.actividad && <Form.Text className="text-danger">Este campo es requerido</Form.Text>}
                                    </div>
                                    {/** ESCOGER FECHA INICIO*/}
                                    <Form.Group controlId="formFechaInicio">
                                        <Form.Label>Fecha de Inicio</Form.Label>
                                        <Form.Control type="datetime-local" {...register('fechaInicio')} />
                                    </Form.Group>
                                    {errors.fechaInicio && <Form.Text className="text-danger">Este campo es requerido</Form.Text>}


                                    {/** ESCOGER FECHA FIN*/}
                                    <Form.Group controlId="formFechaFin">
                                        <Form.Label>Fecha de Fin</Form.Label>
                                        <Form.Control type="datetime-local" {...register('fechaFin')} />
                                    </Form.Group>
                                    {errors.fechaFin && <Form.Text className="text-danger">Este campo es requerido</Form.Text>}


                                    <hr />

                                    {/** BOTÓN CANCELAR */}
                                    <div style={{ display: 'flex', gap: '10px' }}>
                                        <a href="/principal" className="btn btn-danger btn-rounded">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-circle" viewBox="0 0 16 16">
                                                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                                <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                                            </svg>
                                            <span style={{ marginLeft: '5px' }}>Cancelar</span>
                                        </a>

                                        {/** BOTÓN REGISTRAR */}
                                        <input href="/principal" className="btn btn-success btn-rounded" type='submit' value='Registrar'></input>
                                    </div>

                                </form>
                                <hr />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default RegistrarPractica;