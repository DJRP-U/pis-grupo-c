import { useForm } from "react-hook-form";
import { AsignarCurso, Materias, ModificarPersona, ObtenerPersona } from "../hooks/Conexion";
import mensajes from "../utilidades/Mensajes";
import { getToken } from "../utilidades/Sessionutil";
import { useEffect, useState } from "react";

const EditarPersona = ({ row, onCloseModal }) => {
  const { register, handleSubmit, formState: { errors } } = useForm();
  const [llasignar, setLlAsignar] = useState(false);
  const [dato, setDato] = useState({});
  const [materias, setMaterias] = useState([]);
  useEffect(() => {
    Materias(getToken()).then((info) => {
      console.log(info.info);
      if (info.code === 200) {
        setMaterias(info.info);
      }
      
    });
  }, []);
  if (!llasignar) {
    ObtenerPersona(row.external_id, getToken()).then((info) => {
      Asignar(info.info);
      setLlAsignar(true);
    });
  }
  const onSubmit = (data) => {
    console.log(row.external_id);
    console.log(data.materia);
    var datos = {
      "external_persona": row.external_id,
      "external_materia": data.materia
    };

    AsignarCurso(datos, getToken()).then((info) => {
      if (info.code != 200) {
        mensajes(info.msg, 'error', 'Error');
        //msgError(info.message);            
      } else {
        mensajes(info.msg);
      }
    }
    );
  };
  function Asignar(dato) {
    console.log(dato);
  }
  return (
    <div className="p-6">
      <div>
        <form className="user" onSubmit={handleSubmit(onSubmit)}>
          <select id="materia" className="fadeIn second" style={{ width: '100%' }} {...register('materia', { required: true })}>
            <option value="">Seleccione una materia</option>
            {materias.map((materia) => (
              <option key={materia.external_id} value={materia.external_id}>
                {materia.nombre}
              </option>
            ))}
          </select>
          {errors.materia && <div className='alert alert-danger'>Seleccione una materia</div>}

          <button type="submit" className="btn btn-primary btn-lg" > Modificar </button>

        </form>
      </div>
    </div>
  );
}

export default EditarPersona;