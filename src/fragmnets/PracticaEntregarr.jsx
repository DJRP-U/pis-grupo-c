import React from 'react';
import { CalificarEntregable, EntregableIDIDN, EntregableModificar, GuardarEntregable, PracticaDoc, PracticaExternal, URLex } from '../hooks/Conexion';
import { elimianrTodo, eliminarCodigo, eliminarTrabajo, getCodigo, getExPract, getIdenti, saveCodigo, saveExPract } from '../utilidades/ides';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import mensajes from '../utilidades/Mensajes';
import { useNavigate } from 'react-router-dom';
import { getRol, getToken, getValor, saveToken } from '../utilidades/Sessionutil';
import { format } from 'date-fns';
import { Button } from 'react-bootstrap';
import Footer from './Footer';
import Header from './Header';


const PracticaEntregarr = () => {
  const navegation = useNavigate();
  const { register, handleSubmit, formState: { errors } } = useForm();
  const [llasignar, setLlAsignar] = useState(false);
  const [comprobar, setComprobar] = useState(false);
  const [dato, setDato] = useState(null);
  const [doc, setDoc] = useState(null);
  const [traba, setTraba] = useState(null);
  if (!llasignar) {
    //   saveToken('a', 'cHJvZmVzb3I=');
    console.log(atob(getRol()));
    PracticaExternal(getExPract(), getToken()).then((info) => {
      if (info.code !== 200) {
        //&& (info.msg === "No existe token" || info.msg === "Token no valido" )
        elimianrTodo();
        mensajes(info.msg);
        // navegation("/sesion");
      } else {
        //   console.log(info);
        setLlAsignar(true);
        setDato(info.info);
        console.log(getCodigo() + 'no me digas eso c');

        EntregableIDIDN(info.info.id, getValor('identificacion'), getToken()).then((info2) => {
          if (info2.code === 200) {
            console.log(info2.info);
            setDoc(info2.info);
            //   console.log((getCodigo())+"aaaadefe");
            if (info2.info.trabajo !== "" && !getCodigo() && getCodigo() !== '.') {
              saveCodigo(info2.info.trabajo);
              setTraba(getCodigo());
              console.log(getCodigo() + 'comando en obtenr enrter');
            }
          }
        });
      }
    });
  }
  const navegar = () => {
    window.location.href = URLex + dato.documentacion;
  };
  const navegar1 = () => {
    window.location.href = URLex + doc.documentacion;
  };
  const eliminarCOnola = () => {
    saveCodigo('.');
    mensajes("Codigos de consola eliminados");
  };
  const handlePriceChange = (e) => {
    const value = e.target.value.replace(/[^0-9.]/g, '');; // Solo permite números
    if (value !== '' && (value < 0 || value > 10)) {
      // Si el valor está fuera del rango 0-10, ajústalo
      e.target.value = Math.min(10, Math.max(0, value));
    } else {
      e.target.value = value;
    }
  };
  const navegarConsole = () => {
    if (traba) {
      saveCodigo(traba);
    } else if (doc) {
      saveCodigo(doc.trabajo);
    }
    navegation("/consola");
  };
  const onSubmit = (data) => {
    if (atob(getRol()) === 'profesor') {
      var datos = {
        "calificacion": data.precio, // data.detalle, //'{"precio":4000,"cantidad":1,"producto":"dest-de"}'
        "external_id": doc.external_id
      };
      CalificarEntregable(datos, getToken()).then((info) => {
        if (info.code !== 200) {
          mensajes(info.msg, 'error', 'Error');
        } else {
          mensajes(info.msg);
          window.location.reload(); // 
        }
      });
    } else {
      var formData = new FormData();
      if (doc) {
        formData.append("external", doc.external_id);
        if (getCodigo() === '.') {
          formData.append("trabajo", null);
        } else {
          formData.append("trabajo", getCodigo());
        }
        formData.append("file", data.archivo2[0]);
        EntregableModificar(formData, getToken()).then((info) => {
          console.log(doc.external_id);
          if (info.code !== 200) {
            mensajes(info.msg, 'error', 'Error');
          } else {
            mensajes(info.msg);
            eliminarCodigo();
            eliminarTrabajo();
            console.log(getCodigo() + '----elkiminnmods');
            window.location.reload(); // 
          }
        });
      } else {
        formData.append("external_id", dato.external_id);
        formData.append("trabajo", getCodigo());
        //  formData.append("identificacion", getIdenti());
        formData.append("identificacion", getValor('identificacion'));
        formData.append("file", data.archivo[0]);

        GuardarEntregable(formData, getToken()).then((info) => {
          if (info.code !== 200) {
            mensajes(info.msg, 'error', 'Error');
            //msgError(info.message);            
          } else {
            mensajes(info.msg);
            eliminarCodigo();
            eliminarTrabajo();
            console.log(getCodigo() + '---elimin');
            window.location.reload(); // 
          }
        });
      }
    }
    // Accede al archivo seleccionado en el array
  };

  return (
    <div className='fondo-joan'>

      <Header />
      <div>
        <div className="contenedor_completo">
          <div className="wrapper fadeInDown">
            <div id="formContent">
              {dato ? (
                <>
                  <div className="label"></div>
                  <h1>{dato.titulo}</h1>
                  <div className="label"></div>
                  <p>{dato.descripcion}</p>
                  <div className="label"></div>
                  {dato.documentacion !== 'NULL' ? (
                    <input
                      className='Button'
                      type="button"
                      {...register('tiituloText', { required: true })}
                      value={"Documentacion"}
                      onClick={navegar}
                    />
                  ) : (
                    <p>Documento no disponible</p>
                  )}
                  <div className="label">Fecha de limite de entrega:</div>
                  <p>{format(new Date(dato.fechafin), 'yyyy-MM-dd HH:mm')}</p>
                  {doc ? (
                    <div>
                      <div className="label">Estado de la entrega:</div>
                      <p>{doc.calificado === false ? "No calificado" : "Calificado"}</p>
                      {doc.calificado === true && (
                        <div>
                          <div className="label">Calificación:</div>
                          <p>{doc.calificacion}</p>
                        </div>
                      )}
                      <div className="label">Fecha de entrega:</div>
                      <p>{format(new Date(doc.updatedAt), 'yyyy-MM-dd HH:mm')}</p>
                      <h2>{"Estado de entregable"}</h2>
                    </div>
                  ) : (
                    <div></div>

                  )
                  }
                  <div className="w-100" />
                  <form onSubmit={handleSubmit(onSubmit)} className="row row-cols-lg-auto g-3 justify-content-center align-items-center mb-4 pb-2">
                    {doc ? (<div className="col-12">
                      <input
                        className='Button'
                        type="button"
                        {...register('entregablever', { required: true })}
                        value={"Ver entregable"} onClick={navegar1}
                      />
                      <input
                        className='Button'
                        type="button"
                        {...register('verConsola', { required: true })}
                        value={"Ver consola"} onClick={navegarConsole}
                      />
                      {atob(getRol()) === 'estudiante' && (
                        <div>
                          <div className="col-12">
                            Cargar nuevo archivo:  <input type="file"  {...register("archivo2", { required: false })} />
                            {errors.archivo2 && <span>This field is required</span>}
                          </div>

                          <div>
                            <h1></h1>
                            <div className="col-12">
                              <Button type="submit" variant="success" className="btn btn-rounded">Modificar</Button>
                            </div>
                          </div>
                        </div>
                      )}
                    </div>) : (
                      <div>      {traba ? (
                        <>
                          <div className="label">Practica en consola hecha
                            <input
                              className='Button'
                              type="button"
                              {...register('buttonCOsole', { required: true })}
                              value={"Visualizar Consola"} onClick={navegarConsole}
                            />
                            {atob(getRol()) === 'estudiante' && (
                              <input
                                className='Button'
                                type="button"
                                {...register('buttonCOnsole', { required: true })}
                                value={"Eliminar Consola"} onClick={eliminarCOnola}
                                style={{ backgroundColor: 'red', color: 'white' }} // Estilo rojo
                              />
                            )}
                          </div>

                        </>
                      ) : (
                        <div>
                          <input
                            className='Button'
                            type="button"
                            {...register('buttonCOnsole1', { required: true })}
                            value={"Realizar en consola"} onClick={navegarConsole}
                          />
                          <h1></h1>
                        </div>
                      )}
                        {atob(getRol()) === 'estudiante' && (
                          <div>
                            <div className="col-12">
                              Cargar practica:  <input type="file"  {...register("archivo", { required: true })} />
                              {errors.archivo && <span>This field is required</span>}
                            </div>
                            <h1></h1>

                            <div className="col-12">
                              <Button type="submit" variant="success" className="btn btn-rounded">Guardar</Button>
                            </div>
                          </div>
                        )}
                      </div>
                    )
                    }
                    {atob(getRol()) === 'profesor' && (
                      <div>
                        <div className="form-group">
                          <input type="text" className="form-control form-control-user" placeholder="Calificacion" {...register('precio', { required: true })} onChange={handlePriceChange} />
                          {errors.pageCount && errors.pageCount.type === 'required' && <div className='alert alert-danger'>Ingrese calificacion</div>}
                        </div>
                        <div className="col-12">
                          <Button type="submit" variant="success" className="btn btn-rounded">Calificar</Button>
                        </div>
                      </div>
                    )}
                  </form>
                </>
              ) : (
                <p>Cargando datos...</p>
              )}
            </div>
          </div>
        </div>
      </div>

      <Footer />
    </div>


  );
};

export default PracticaEntregarr;
