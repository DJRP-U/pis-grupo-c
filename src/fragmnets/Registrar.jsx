import Footer from "./Footer";
import "bootstrap/dist/css/bootstrap.min.css";
import '../css/styleLogin.css';
import { GuardarPersona, InicioSesion, ListarRoles } from '../hooks/Conexion';
import { useNavigate } from 'react-router';
import { useForm } from 'react-hook-form';
import mensajes from '../utilidades/Mensajes';
import { getToken, saveToken } from '../utilidades/Sessionutil';
import { useState } from "react";


const Registrar = () => {
    const navegation = useNavigate();
    const { register, handleSubmit, formState: { errors } } = useForm();

    const [roles, setRoles] = useState([]);
    const [llroles, setLlroles] = useState(false)

    const tiposIden = ['CEDULA', 'PASAPORTE', 'RUC']

    if (!llroles) {
        ListarRoles().then((info) => {
            setRoles(info.info);
            setLlroles(true);
        })
    }

    const onSubmit = (data) => {
        var datos = {
            "correo": data.correo,
            "clave": data.clave,
            "identificacion": data.identificacion,
            "apellidos": data.apellidos,
            "nombres": data.nombres,
            "tipo_identificacion": data.tipo_identificacion,
            "external_rol": data.rol,
            "direccion": data.direccion
        };
        GuardarPersona(getToken(), datos).then((info) => {
            if (info.code !== 200) {
                console.log(info.code)
                console.log("Entro a !== 200")
                mensajes(info.message, 'error', 'Error');
            } else {
                console.log(info)
                mensajes(info.message);
                navegation('/login');
            }
        })
    };
    return (
        <div class="animation-area">
            <div className="p-6">
                <br />
                <div align="left" className="botonRegresar"><a href="/principal"><button className="btn btn-secondary" >Regresar</button></a></div>
                <div className="contenedor_completo">
                    <div className="wrapper fadeInDown">
                        <div id="formContent">
                            <div className="fadeIn first">
                                <img src="https://upload.wikimedia.org/wikipedia/commons/d/df/UNL3.png" id="icon" alt="User Icon" ></img>
                            </div>
                            <br />
                            <form onSubmit={handleSubmit(onSubmit)}>
                                <div>
                                    <input type="text" id="nombres" className="fadeIn second" name="nombres" placeholder="Nombres" {...register('nombres', { required: true })} />
                                    {errors.nombres && errors.nombres.type === 'required' && <div className='alert alert-danger'>Ingrese sus nombres</div>}
                                    {errors.nombres && errors.nombres.type === 'pattern' && <div className='alert alert-danger'>Ingrese nombres válidos</div>}
                                    <br />
                                    <input type="text" id="apellidos" className="fadeIn second" name="apellidos" placeholder="Apellidos" {...register('apellidos', { required: true })} />
                                    {errors.apellidos && errors.apellidos.type === 'required' && <div className='alert alert-danger'>Ingrese sus apellidos</div>}
                                    {errors.apellidos && errors.apellidos.type === 'pattern' && <div className='alert alert-danger'>Ingrese apellidos válidos</div>}
                                    <br />
                                    <div>
                                        <select className='fadeIn second' {...register('tipo_identificacion', { required: true })}>
                                            <option disabled>Identificación</option>
                                            {tiposIden.map((aux, i) => {
                                                return (<option key={i} value={aux}>
                                                    {aux}
                                                </option>)
                                            })}
                                        </select>
                                        {errors.marca && errors.marca.type === 'required' && <div className='alert alert-danger'>Selecione una marca</div>}
                                    </div>
                                    <br />
                                    <input type="text" id="identificacion" className="fadeIn second" name="identificacion" placeholder="Identificacion" {...register('identificacion', { required: true, pattern: /^[0-9]{10}$/ })} />
                                    {errors.identificacion && errors.identificacion.type === 'required' && <div className='alert alert-danger'>Ingrese su identificacion</div>}
                                    {errors.identificacion && errors.identificacion.type === 'pattern' && <div className='alert alert-danger'>Ingrese una identificacion valida</div>}
                                    <br />
                                    <input type="text" id="direccion" className="fadeIn second" name="direccion" placeholder="Dirección" {...register('direccion', { required: true })} />
                                    {errors.direccion && errors.direccion.type === 'required' && <div className='alert alert-danger'>Ingrese su direccion</div>}
                                    {errors.direccion && errors.direccion.type === 'pattern' && <div className='alert alert-danger'>Ingrese una direccion valida</div>}
                                    <br />
                                    <div>
                                        <select className='fadeIn second' {...register('rol', { required: true })}>
                                            <option disabled>Elija un rol</option>
                                            {roles.map((aux, i) => {
                                                return (<option key={i} value={aux.external_id}>
                                                    {aux.nombre}
                                                </option>)
                                            })}
                                        </select>
                                        {errors.marca && errors.marca.type === 'required' && <div className='alert alert-danger'>Selecione una marca</div>}
                                    </div>
                                    <input type="text" id="correo" className="fadeIn second" name="correo" placeholder="Correo" {...register('correo', { required: true, pattern: /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/ })} />
                                    {errors.correo && errors.correo.type === 'required' && <div className='alert alert-danger'>Ingrese el correo</div>}
                                    {errors.correo && errors.correo.type === 'pattern' && <div className='alert alert-danger'>Ingrese un correo valido</div>}
                                    <br />
                                    <input type="password" id="clave" className="fadeIn third" name="clave" placeholder="Contraseña" {...register('clave', { required: true })} />
                                    {errors.clave && errors.clave.type === 'required' && <div className='alert alert-danger'>Ingrese una clave</div>}
                                    <br />
                                    <br />
                                    <button type="submit" className="btn btn-primary btn-lg" style={{background: '#162b4e'}}>Registrar </button>

                                    <br />
                                    <br />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <ul class="box-area">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
            </div>
        </div>

    );
}

export default Registrar;