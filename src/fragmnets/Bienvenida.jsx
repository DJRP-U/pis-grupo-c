
import "bootstrap/dist/css/bootstrap.min.css";
import '../css/styleLogin.css';
import '../css/textoAnimate.css'

const Bienvenida = () => {

    return (
        <div className="p-6">
            <div>
                <div className="contenedor_completo">
                    <div className="wrapper fadeInDown">
                        <div id="formContent">
                            <div className="fadeIn first">
                                <div class="animated-text">
                                    <span>Bienvenido Usuario a esta Materia</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Bienvenida;