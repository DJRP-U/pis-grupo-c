import '../css/Dashboard.css';
import React, { useState } from 'react';

import {
    FaTh,
    FaBars,
    FaThList,
    FaTasks
} from "react-icons/fa";
import {
    BsMortarboardFill,
    BsFillPeopleFill
} from "react-icons/bs";

import Header from './Header';
import Consola from './Consola';
import Materia from './Materia';
import { MateriasCursa, ObtenerMateria } from '../hooks/Conexion';
import { getRol, getToken } from '../utilidades/Sessionutil';
import { getMateria } from '../utilidades/ides';
import Footer from './Footer';
import PresentarPractica from './PresentarPractica';
import PresentarPracticaEStudiante from './PresentarPracticaEStudiante';
import Participantes from './Participantes';
import Bienvenida from './Bienvenida';


const Sidebar = () => {
    const [isOpen, setIsOpen] = useState(false);
    const [selectedOption, setSelectedOption] = useState(<Bienvenida/>);
    const toggle = () => setIsOpen(!isOpen);
    const [itemMateria, setitemMateria] = useState({});
    const [llmateria, setLlmaterias] = useState(false);

    if (!llmateria) {
        ObtenerMateria(getMateria(), getToken()).then((info) => {
            console.log(info);
            if (info.code === 200) {
                console.log(info.info);
                setitemMateria(info.info);
            }
            setLlmaterias(true);
        });
    }
    const menuItem = [
        {
            alias: "Materia",
            name: itemMateria.nombre,
            icon: <BsMortarboardFill />
        },
        {
            path: "/",
            alias: "Practicas",
            name: "Practicas",
            icon: <FaTasks />
        },
        {
            path: "/consola",
            alias: "Participantes",
            name: "Participantes",
            icon: <BsFillPeopleFill />
        }/*,
        {
            path: "/analytics",
            alias: "Reporte",
            name: "Reporte",
            icon: <FaThList />
        }*/
    ];
    /*const menuItem = [];
    var check = true;

    for (const item of itemMaterias) {
        const name = item.nombre;
        const external_id = item.external_id;
        const icon = <BsMortarboardFill />;
        menuItem.push({ name, external_id, icon});

        if (check) {
            for (const item of submenuItem) {
                const name = item.name;
                const icon = item.icon;
                menuItem.push({ name, icon });
            }
            check = false;
        }
        
    }*/

    const components = {
        //if(atob(getRol())=="profesor"){
        //    navegation('/practicadoc') <PresentarPractica />,
        //  } else if(atob(getRol())=="estudiante"){
        //    navegation('/practica/estudiante') <PresentarPracticaEStudiante/>
        //  } <Bienvenida/>
        Materia: <Bienvenida/>,
        Participantes: atob(getRol()) === "profesor" || atob(getRol()) === "estudiante" ? <Participantes /> :<div><br/><br/><br/><br/><h2>No tiene nada que hacer aquí</h2></div>,
        Practicas: atob(getRol()) === "profesor" ? <PresentarPractica /> : atob(getRol()) === "estudiante"? <PresentarPracticaEStudiante />:<div><br/><br/><br/><br/><h2>No tiene nada que hacer aquí</h2></div>,
        //Reporte: <Consola />
    };



    return (
        <div class="fondo-joan"> 
            <Header className="header" />
            <div className="container">
                <div style={{ width: isOpen ? "200px" : "50px" }} className="sidebar">
                    <div className="top_section">
                        <h1 style={{ display: isOpen ? "block" : "none" }} className="logo">UNL  </h1>
                        <div style={{ marginLeft: isOpen ? "50px" : "0px" }} className="bars">
                            <FaBars onClick={toggle} />
                        </div>
                    </div>
                    {
                        menuItem.map((item, index) => (
                            <div key={index} className="link" onClick={() => setSelectedOption(item.alias)} activeclassName="active">
                                <div className="icon">{item.icon}</div>
                                <div style={{ display: isOpen ? "block" : "none" }} className="link_text">{item.name}</div>
                            </div>
                        ))
                    }
                </div>
                <main>{components[selectedOption]}</main>
            </div>    
        </div>
    );
};

export default Sidebar;