import Header from "./Header";
import Footer from "./Footer";
import '../css/style.css';
import '../css/MyDataTable.css'
import DataTable, { createTheme } from "react-data-table-component";
import { Button, Modal } from 'react-bootstrap';
import { useEffect, useState } from "react";
import { Materias, MateriasParticipantes } from "../hooks/Conexion";
import { getToken, getValor } from "../utilidades/Sessionutil";
import RegistroMateria from "./RegistroMateria";
import EditarMateria from "./EditarMateria";
import { getMateria } from "../utilidades/ides";

const Participantes = () => {

    const [llmateria, setLlmaterias] = useState(false);
    const [materias, setMaterias] = useState([]);
    const [searchTerm, setSearchTerm] = useState('');
    const [filteredData, setFilteredData] = useState(materias); // data es tu array de datos original

    const handleSearch = () => {
        const filteredData = materias.filter(item => {
            const nombreMatches = item.nombre.toLowerCase().includes(searchTerm.toLowerCase());
            const apellidoMatches = item.apellido.includes(searchTerm.toLowerCase());
            const rolMatches = item.rol.includes(searchTerm.toLowerCase());

            return nombreMatches || apellidoMatches || rolMatches;
        });
        setFilteredData(filteredData);
    };

    useEffect(() => {
        handleSearch();
    }, [searchTerm]);

    if (!llmateria) {
        MateriasParticipantes(getMateria(),getToken()).then((info) => {
            console.log(info);
            if (info.code === 200) {
                console.log(info.info);
                setMaterias(info.info);
                setFilteredData(info.info);
            }
            setLlmaterias(true);
        });
    }

    const columns = [
        {
            name: 'Nombre',
            selector: row => row.nombre,
            wrap: true
        },
        {
            name: 'Apellido',
            selector: row => row.apellido,
            wrap: true
        },{
            name: 'Rol',
            selector: row => row.rol,
            wrap: true
        }
    ];

    const customStyles = {
        rows: {
            style: {
                minHeight: '72px', // override the row height
                fontFamily: 'Arial, sans-serif',
            },
        },
        headCells: {
            style: {
                paddingLeft: '8px', // override the cell padding for head cells
                paddingRight: '8px',
                fontSize: '18px',
                fontFamily: 'Arial, sans-serif',
            },
        },
        cells: {
            style: {
                paddingLeft: '8px', // override the cell padding for data cells
                paddingRight: '8px',
                fontSize: '16px',
            },
        },
    };

    return (
        <div className="wrapper">
            <Header />
            <div className="d-flex flex-column">
                <div className="content">

                    {/** DE AQUI CUERPO */}
                    <h2>Participantes de Esta Materia</h2>
                    <div className='container-fluid'>
                        <input
                            type="text"
                            placeholder="A quién Buscas?"
                            value={searchTerm}
                            onChange={event => setSearchTerm(event.target.value)}
                        />
                        <DataTable
                            className="my-data-table"
                            striped
                            fixedHeader
                            columns={columns}
                            data={filteredData}
                            responsive={true}
                            highlightOnHover={true}
                        />
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
}

export default Participantes;