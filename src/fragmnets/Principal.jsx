import '../css/style.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import { Badge, ListGroup, Navbar, Tab, Tabs } from 'react-bootstrap';
import Activities from '../fragmnets/Activities'
import Header from './Header';
import Footer from './Footer';
import RegistrarPractica from './RegistrarPractica';

const Principal = () => {
  return (
    <div className='fondo-joan'>
      <Header />
      <br></br>
      <div className="contenedor_completo">
        <div className="wrapper fadeInDown">
          <div id="formContent">
            <div className="fadeIn first">
              <div class="animated-text">
                <span>Bienvenido a Nuestro Sistema</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <br></br>
      <Footer />
    </div>

  );
}

export default Principal;