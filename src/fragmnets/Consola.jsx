import React, { useState, useRef, useEffect } from 'react';
import AceEditor from "react-ace";
import "ace-builds/src-noconflict/theme-monokai";
import "ace-builds/src-noconflict/mode-text";
import "ace-builds/src-noconflict/theme-monokai";
import '../css/style.css';
import Header from './Header';
import { Button, Modal } from 'react-bootstrap';
import { useNavigate } from 'react-router';
import Footer from './Footer';
import { useForm } from 'react-hook-form';
import { eliminarCodigo, getCodigo, getExPract, getIdenti, saveCodigo, saveExPract, saveTrabajo } from '../utilidades/ides';
import { GuardarEntregable, ObtenerCobsola } from '../hooks/Conexion';
import mensajes from '../utilidades/Mensajes';
import { getRol, getToken } from '../utilidades/Sessionutil';
import { set } from 'date-fns';

const commands = {
  echo: {
    description: 'Echo a passed string.',
    usage: 'echo <string>',
    fn: (...args) => args.join(' ')
  }
};

const caracter = '>';

const Consola = () => {
  const aceEditorRef = useRef(null);
  const navegation = useNavigate();
  const [show, setShow] = useState(false);
  const [output, setoutput] = useState(false);
  const [commandHistory, setCommandHistory] = useState([]);
  const [commandHistoryT, setCommandHistoryT] = useState([]);
  const { register, handleSubmit, formState: { errors }, reset } = useForm();
  const consoleRef = useRef(null);
  const [llasignar, setLlAsignar] = useState(false);
  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);
  const [traba, setTraba] = useState(null);
  if (!llasignar) {
    const codigoString = getCodigo();
    console.log(codigoString);
    if (codigoString) { // Verifica si el código no es nulo o vacío
      try {
        const parsedCodigo = JSON.parse(codigoString);
        const parsedCodigo1 = JSON.parse(parsedCodigo);
        console.log("codigoString");
        // Verifica si el objeto es un arreglo
        for (let index = 0; index < parsedCodigo1.length; index++) {
          setCommandHistory((prevHistory) => [...prevHistory, parsedCodigo1[index].comando]);
          setCommandHistory((prevHistory) => [...prevHistory, parsedCodigo1[index].resultado]);
          setCommandHistoryT((prevHistory) => [...prevHistory, parsedCodigo1[index].tiempo]);
        }
      } catch (error) {
        console.log('Error al analizar el código JSON:', error);
      }
    }

    eliminarCodigo();
    setLlAsignar(true);
  }
  const scrollToBottom = () => {
    if (aceEditorRef.current) {
      const editorInstance = aceEditorRef.current.editor;
      const lastLine = editorInstance.session.getLength() - 1;
      editorInstance.scrollToLine(lastLine, true, true, () => { });
    }
  };
  const guardar = () => {
    let traba = "[";
    let traba1 = "";
    for (let index = 0, j = 0; index < commandHistory.length; j++) {
      traba1 = traba1 + '{\"comando\":\"' + commandHistory[index] + "\",\"resultado\":\"" + commandHistory[index + 1] + "\",\"tiempo\":\"" + commandHistoryT[j] + "\"}";
      if (index < commandHistory.length - 2) {
        traba1 = traba1 + ",";
      }
      index = index + 2;
    }
    let conactenar = traba + traba1 + "]";
    console.log(conactenar);
    saveCodigo(conactenar);
    navegation('/visualizar/practica');
    /*   var datos = {
         "trabajo": conactenar, // data.detalle, //'{"precio":4000,"cantidad":1,"producto":"dest-de"}'
         "identificacion": getIdenti(),
         "external_id": getExPract()
       };
       console.log('a--------------------aaaaaa'+ getCodigo());
     GuardarEntregable(datos).then((info) => {
         if (info.code !== 200) {
           mensajes(info.msg, 'error', 'Error');
           //msgError(info.message);            
         } else {
           mensajes(info.msg);
           //  navegation('/inicio');
         }
       });*/
  };
  const volver = () => {

  };


  useEffect(() => {
    scrollToBottom();
    //   saveCodigo(commandHistory);
    //  console.log(getCodigo());
  }, [commandHistory]);

  const onSubmit = async (data) => {
    const command = data.tiituloText.trim();
    if (command !== '' && command !== '\\') {
      const commandEncoded = command.replace(/%/g, '%25').replace(/\//g, '%2F').replace(/"/g, '%22').replace(/'/g, '%27').replace(/\\/g, '%5C').replace(/#/g, '%23').replace(/\?/g, '%3F');
      var comnan = commandEncoded;
      console.log('---' + commandHistory);
      setCommandHistory((prevHistory) => [...prevHistory, comnan]);
      reset();
      ObtenerCobsola(comnan, getToken()).then((info) => {
        //  saveExPract("9cba9211-97ca-4d4d-af8f-d2715bb17a74");
        console.log((info));
        setCommandHistory((prevHistory) => [...prevHistory, (info.info)]);
        setCommandHistoryT((prevHistory) => [...prevHistory, ("Respuesta en:" + info.time + "ms")]);
        console.log(commandHistory);
      });
    }
  };

  const handleCancel = () => {
    if (atob(getRol()) === 'estudiante') {
      confirmCancel();
    }
    saveCodigo('.');
    navegation("/visualizar/practica");
  };

  /*
   <Modal show={show} onHide={handleClose} centered>
          <Modal.Header closeButton>
            <Modal.Title>Cancelar práctica</Modal.Title>
          </Modal.Header>
          <Modal.Body>¿Estás seguro que deseas cancelar la práctica?</Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Cerrar
            </Button>
            <Button variant="danger" onClick={confirmCancel}>
              Cancelar práctica
            </Button>
          </Modal.Footer>
        </Modal>
  
  */

  const confirmCancel = () => {
    setCommandHistory([]);
    handleClose();
  };

  return (
    <div >
      <Header />
    
      {/*
      <div className='container'>
        <div className="bodyConsole">
          <div className='consoleYovin' ref={consoleRef}>
            {commandHistory.map((command, index) => (
              <div key={index} className="command"> {caracter}{command}</div>
            ))}
          </div>
        </div>
        <div id="console"></div>
          
      </div>  */}

      <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '50vh', marginTop: "99px" }}>

        <AceEditor
          mode="text"
          theme="monokai"
          name="output-editor"
          editorProps={{ $blockScrolling: Infinity }}
          value={commandHistory.map(command => `${caracter}${command}`).join('\n')}
          readOnly={true}
          style={{ width: '100%', background: '#272822', color: '#FFFFFF' }}
          ref={aceEditorRef}
        />
      </div>
      {atob(getRol()) === 'estudiante' && (
        <div>
          <div className='consoleInput' style={{ margin: "30px auto 0", width: "100%", maxWidth: "75vh" }}>
            <form onSubmit={handleSubmit(onSubmit)}>
              <input
                className='consoleInput'
                type="text"
                {...register('tiituloText', { required: true })}
                placeholder="Ingresar comando (Presione enter)"
              />
              {errors.tiituloText && errors.tiituloText.type === 'required' && (
                <div className='alert alert-danger'>Ingresar un comando</div>
              )}
            </form>
          </div>
          <div className="col-sm-3 offset-sm-1 mt-5 mb-4 text-gred">
            <Button className="mr-2" onClick={guardar}>
              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16">
                <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
              </svg>
              <span style={{ marginLeft: '5px' }}>Entregar Práctica</span>
            </Button>
            <div className="col-12">
              <Button onClick={handleCancel} variant="danger">Cancelar</Button>
            </div>
          </div></div>
      )}
      {atob(getRol()) === 'profesor' && (
        <div className="col-sm-3 offset-sm-1 mt-5 mb-4 text-gred">
          <Button className="mr-2" onClick={handleCancel}>
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16">
              <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
            </svg>
            <span style={{ marginLeft: '5px' }}>Terminar revision</span>
          </Button>
        </div>
      )}
  
      <Footer/>
    </div>

  );
};

export default Consola;
