import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Modal } from 'react-bootstrap';
import DataTable from "react-data-table-component";
import React, { useState } from 'react';
import { borrarSesion, getToken } from "../utilidades/Sessionutil";
import mensajes from "../utilidades/Mensajes";
import { useNavigate } from "react-router";
import { format } from 'date-fns';
import { EntregablePractica, PracticaEliminar, PracticaMateria } from "../hooks/Conexion";
import AgregarPracticas from "./AgregarPracticas";
import { getExPract, saveExPract, saveIdenti } from "../utilidades/ides";
import Header from "./Header";
import Footer from "./Footer";
const ExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;
const columns = [
    {
        name: 'Nombre',
        selector: row => row.persona.nombres + " " + row.persona.apellidos,
        wrap: true,
    },

    {
        name: 'Identificacion',
        selector: row => row.persona.identificacion,
        wrap: true,
    },
    {
        name: 'Fecha entrega',
        selector: row => format(new Date(row.updatedAt), 'yyyy-MM-dd HH:mm'),
        wrap: true,
    }, {
        name: 'Calificado',
        selector: row => (row.calificado ? 'Calificado' : 'No Calificado'),
        wrap: true,
    },
    {
        name: 'Calificacion',
        selector: row => row.calificacion,
        wrap: true,
    },
    {
        /* 
           {
         name: 'Precio',
         selector: row => '$ ' + row.precio,
         wrap: true,
     },
        
        name: 'Acciones',
         selector: row => (<>
         
             <div style={{ display: 'flex', gap: '10px' }}>
                 <a href="/autos/edicion" class="btn btn-outline-info btn-rounded">
                     <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-pencil-square" viewBox="0 0 16 16">
                         <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
                         <path fillRule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z" />
                     </svg>
                 </a>
 
                 <a href="/autos" className="btn btn-outline-danger btn-rounded">
                     <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-x-circle" viewBox="0 0 16 16">
                         <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                         <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                     </svg>
                 </a>
             </div>
             
         </>),*/
    },

];


export const PresentarEntregables = () => {
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const [data, setData] = useState([]);
    const navegation = useNavigate();
    const [llLIbros, llsetLibros] = useState(false);
    const [searchText, setSearchText] = useState('');
    const [filteredData, setFilteredData] = useState([]);
    const [selectedBook, setSelectedBook] = useState(null);
    const [numeroFila, setNumeroFila] = useState(0);
    if (!llLIbros) {
        //getMateria
        EntregablePractica(getExPract(), getToken()).then((info) => {
            console.log(info);
            if (info.code !== 200 && (info.msg === "No existe token" || info.msg === "Token no valido")) {
                borrarSesion();
                mensajes(info.msg);
                navegation("/sesion")
            } else {
                console.log(info);
                llsetLibros(true);
                setData(info.info);
            }
        })

    }




    const handleBookClick = (data) => {
        setSelectedBook(data);
        //  setNumeroFila(data.placa);
        saveIdenti(data.persona.identificacion);
        //   console.log(getID());
        handleShow(true);
        navegation('/visualizar/practica');

    };
    const handleSearch = (e) => {
        const searchValue = e.target.value;
        setSearchText(searchValue);
        const filteredBooks = data.filter(book =>
            book.title.toLowerCase().includes(searchValue.toLowerCase()));
        setFilteredData(filteredBooks);
    };

    return (
        <div className="fondo-joan">
            <div className="container">
                <Header />
                <br />
                <br />
                <br />
                <br />
                <div className="crud shadow-lg p-3 mb-5 mt-5 bg-body rounded">
                    <div className="row ">

                        <div className="col-sm-3 mt-5 mb-4 text-gred">

                        </div>
                        <div className="col-sm-3 offset-sm-2 mt-5 mb-4 text-gred" style={{ color: "blue" }}>
                            <h2><b>Entregables</b></h2>
                            <p style={{ color: "black" }}>Seleccionar entregable para calificar </p>
                        </div>

                    </div>
                    <div className="row">

                        <DataTable
                            columns={columns}
                            data={data}
                            selectableRows
                            onRowClicked={handleBookClick}
                        /> <div className="model_box">
                            <Modal
                                show={show}
                                onHide={handleClose}
                                backdrop="static"
                                keyboard={false}
                            > <Modal.Body>
                                </Modal.Body>

                                <Modal.Body>

                                </Modal.Body>

                                <Modal.Footer>
                                    <Button variant="secondary" onClick={handleClose}>
                                        Cerrar
                                    </Button>

                                </Modal.Footer>
                            </Modal>
                        </div> </div></div>


            </div>
            <Footer />
        </div>
    );
}


export default PresentarEntregables;