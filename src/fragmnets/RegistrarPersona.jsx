import React, { useState } from 'react';

const   RegistroPersonas = () => {
  const [nombre, setNombre] = useState('');
  const [apellido, setApellido] = useState('');
  const [edad, setEdad] = useState('');
  const [correo, setCorreo] = useState('');
  const [clave, setClave] = useState('');

  const handleNombreChange = (e) => {
    setNombre(e.target.value);
  };

  const handleApellidoChange = (e) => {
    setApellido(e.target.value);
  };

  const handleEdadChange = (e) => {
    setEdad(e.target.value);
  };

  const handleCorreoChange = (e) => {
    setCorreo(e.target.value);
  };

  const handleClaveChange = (e) => {
    setClave(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    // Aquí puedes realizar la lógica de envío o almacenamiento de los datos de la persona
    console.log('Nombre:', nombre);
    console.log('Apellido:', apellido);
    console.log('Edad:', edad);
    console.log('Correo:', correo);
    console.log('Clave:', clave);
    // Luego puedes reiniciar los campos del formulario
    setNombre('');
    setApellido('');
    setEdad('');
    setCorreo('');
    setClave('');
  };

  return (
    <div>
      <h1>Registro de Personas</h1>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="nombre">Nombre:</label>
          <input
            type="text"
            id="nombre"
            className="form-control"
            value={nombre}
            onChange={handleNombreChange}
          />
        </div>
        <div className="form-group">
          <label htmlFor="apellido">Apellido:</label>
          <input
            type="text"
            id="apellido"
            className="form-control"
            value={apellido}
            onChange={handleApellidoChange}
          />
        </div>
        <div className="form-group">
          <label htmlFor="edad">Edad:</label>
          <input
            type="number"
            id="edad"
            className="form-control"
            value={edad}
            onChange={handleEdadChange}
          />
        </div>
        <div className="form-group">
          <label htmlFor="correo">Correo:</label>
          <input
            type="email"
            id="correo"
            className="form-control"
            value={correo}
            onChange={handleCorreoChange}
          />
        </div>
        <div className="form-group">
          <label htmlFor="clave">Clave:</label>
          <input
            type="password"
            id="clave"
            className="form-control"
            value={clave}
            onChange={handleClaveChange}
          />
        </div>
        <button type="submit" className="btn btn-primary">Registrar</button>
      </form>
    </div>
  );
};

export default RegistroPersonas;

