import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { Button } from 'react-bootstrap';
import Header from './Header';
import Footer from './Footer';
import { GuardarPractica, ModificarPractica, PracticaExternal, PracticaMateria } from '../hooks/Conexion';
import { borrarSesion, getToken } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensajes';
import { useNavigate } from 'react-router';
import { eliminarConfir, getConfir, getExPract, getMateria } from '../utilidades/ides';
import { getConfig } from '@testing-library/react';

const AgregarPracticas = () => {
    const { register, handleSubmit, formState: { errors } } = useForm();
    const [data, setData] = useState([]);
    const navegation = useNavigate();
    const [datas, setDatas] = useState(null);
    const [llLIbros, llsetLibros] = useState(false);
    if (!llLIbros) {
        //getMateria
        if (getConfir()) {
            PracticaExternal(getExPract(), getToken()).then((info) => {
                console.log(info);
                if (info.code !== 200 && (info.msg === "No existe token" || info.msg === "Token no valido" || info.msg === "Token no existe")) {
                    borrarSesion();
                    mensajes(info.msg);
                    navegation("/sesion")
                } else {
                    console.log(info.info);
                    llsetLibros(true);
                    setDatas(info.info);
                }
            })
        }
    }

    const handleFormSubmit = (data) => {
        // Lógica para guardar los datos del formulario, incluyendo el archivo
        console.log(data);
    };

    const onSubmit = (data) => {
        var formData = new FormData();
if (!datas) {
    formData.append("external_id", getMateria()); 
    formData.append("titulo", data.titulo);
    formData.append("descripcion", data.descripcion);
    formData.append("actividad", 'PENDIENTE');
    formData.append("file", data.archivo[0]);
    formData.append("fechaFin", data.fechaEntrega);
    
    GuardarPractica(formData, getToken()).then((info) => {
        if (info.code !== 200) {
            mensajes(info.msg, 'error', 'Error');
        } else {
            mensajes(info.msg);
            // navegation('/inicio');
            window.location.reload(); // 
        }
    });
}else{
    formData.append("titulo", data.titulo1);
    formData.append("external", datas.external_id);
    formData.append("descripcion", data.descripcion1);
    formData.append("actividad", 'PENDIENTE');
    formData.append("file", data.archivo1[0]);
    formData.append("fechaFin", data.fechaEntrega1);
    console.log(formData);
    ModificarPractica(formData, getToken()).then((info) => {
        if (info.code !== 200) {
            mensajes(info.msg, 'error', 'Error');
        } else {
            mensajes(info.msg);
            eliminarConfir();
            navegation('/sidebar');
        }
    });
}
 // Accede al archivo seleccionado en el array
    //    console.log(formData);
       
    };

    const handleCancel = () => {
        eliminarConfir();
        navegation("/principal");
    };
    /*   const onSubmit = (data) => {
           var datos = {
               "titulo": data.titulo,
               "descripcion": data.descripcion,
               "actividad": 'PENDIENTE',
               "file": data.archivo,
               "fechaFin": data.fechaEntrega
           };
           console.log(data.archivo);
           GuardarPractica(datos, getToken(), data.archivo).then((info) => {
               if (info.code !== 200) {
                   mensajes(info.msg, 'error', 'Error');
                   //msgError(info.message);            
               } else {
                   mensajes(info.msg);
                 //  navegation('/inicio');
               }
           });
       };
   */
    return (
        <div >
            <Header />
          
                             
            <section className="vh-100">
                <div className="container py-5 h-100">
                    <div className="row d-flex justify-content-center align-items-center h-100">
                        <div className="col col-lg-9 col-xl-7">
                            <div className="card rounded-3">
                                <div className="card-body p-4">
                                    {datas ? (<form onSubmit={handleSubmit(onSubmit)} className="row row-cols-lg-auto g-3 justify-content-center align-items-center mb-4 pb-2">
                                        <div className="col-12">
                                            Titulo    <input type="text" {...register("titulo1", { required: true })} className="form-control" placeholder="Título" defaultValue={datas.titulo}/>
                                            {errors.titulo && <span>This field is required</span>}
                                        </div>

                                        {/* Salto de línea */}
                                        <div className="w-100" />

                                        <div className="col-12">
                                            Descripcion: <textarea {...register("descripcion1", { required: false })} className="form-control" placeholder="Descripción" defaultValue={datas.descripcion}/>
                                            {errors.descripcion1 && <span>This field is required</span>}
                                        </div>
                                        <div className="w-100" />
                                        <div className="col-12">
                                           Nueva documentacion:  <input type="file"  {...register("archivo1", { required: false })} />
                                            {errors.archivo && <span>This field is required</span>}
                                        </div>
                                        <div className="w-100" />
                                        <div className="col-12"> Fecha de entrega:  <input type="datetime-local" {...register("fechaEntrega1", { required: true })} defaultValue={datas.fechaFin}/>
                                            {errors.fechaEntrega && <span>This field is required</span>}
                                        </div>
                                        <div className="w-100" />
                                        <div className="col-12">
                                            <Button type="submit" variant="success" className="btn btn-rounded">Editar</Button>
                                        </div>

                                        <div className="col-12">
                                            <Button onClick={handleCancel} variant="danger">Cancelar</Button>
                                        </div>
                                    </form>) : (<form onSubmit={handleSubmit(onSubmit)} className="row row-cols-lg-auto g-3 justify-content-center align-items-center mb-4 pb-2">
                                        <div className="col-12">
                                            Titulo    <input type="text" {...register("titulo", { required: true })} className="form-control" placeholder="Título" />
                                            {errors.titulo && <span>This field is required</span>}
                                        </div>

                                        {/* Salto de línea */}
                                        <div className="w-100" />

                                        <div className="col-12">
                                            Descripcion: <textarea {...register("descripcion", { required: false })} className="form-control" placeholder="Descripción" />
                                            {errors.descripcion && <span>This field is required</span>}
                                        </div>
                                        <div className="w-100" />
                                        <div className="col-12">
                                            Documentacion:  <input type="file"  {...register("archivo", { required: false })} />
                                            {errors.archivo && <span>This field is required</span>}
                                        </div>
                                        <div className="w-100" />
                                        <div className="col-12"> Fecha de entrega:  <input type="datetime-local" {...register("fechaEntrega", { required: true })} />
                                            {errors.fechaEntrega && <span>This field is required</span>}
                                        </div>
                                        <div className="w-100" />
                                        <div className="col-12">
                                            <Button type="submit" variant="success" className="btn btn-rounded">Guardar</Button>
                                        </div>

                                        <div className="col-12">
                                            <Button onClick={handleCancel} variant="danger">Cancelar</Button>
                                        </div>
                                    </form>)}

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
           
            <Footer />
        </div>
    );
};

export default AgregarPracticas;
