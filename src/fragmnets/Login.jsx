import Footer from "./Footer";
import "bootstrap/dist/css/bootstrap.min.css";
import '../css/styleLogin.css';
import { InicioSesion } from '../hooks/Conexion';
import { useNavigate } from 'react-router';
import { useForm } from 'react-hook-form';
import mensajes from '../utilidades/Mensajes';
import { saveToken, saveValor } from '../utilidades/Sessionutil';

const Login = () => {
    const navegation = useNavigate();
    const { register, handleSubmit, formState: { errors } } = useForm();

    const onSubmit = (data) => {

        var datos = {
            "correo": data.correo,
            "clave": data.clave,
        };
        console.log(datos);
        InicioSesion(datos).then((info) => {

            if (info.code !== 200) {
                console.log(info.code)
                console.log("Entro a !== 200")
                mensajes(info.msg, 'error', 'Error');
            } else {
                if (info.token) {
                    console.log(info)
                    saveToken(info.token, info.rol);
                    saveValor('nombre', info.user);
                    saveValor('external', info.external);
                    saveValor('identificacion', info.iden);
                    console.log(info.token);
                    mensajes(info.msg);
                    navegation('/principal');
                } else {
                    mensajes(info.msg, 'error', 'Error');
                }

            }
        }
        );
    };
    return (
        <div className="p-6">
            <div class="animation-area">
                <div className="contenedor_completo">
                    <div className="wrapper fadeInDown">
                        <div id="formContent">
                            <div className="fadeIn first">
                                <img src="https://upload.wikimedia.org/wikipedia/commons/d/df/UNL3.png" id="icon" alt="User Icon" ></img>
                            </div>
                            <br />
                            <form onSubmit={handleSubmit(onSubmit)}>
                                <div>
                                    <input type="text" id="correo" className="fadeIn second" name="correo" placeholder="Usuario" {...register('correo', { required: true, pattern: /^\S+@\S+$/i })} />
                                    {errors.correo && errors.correo.type === 'required' && <div className='alert alert-danger'>Ingrese el correo</div>}
                                    {errors.correo && errors.correo.type === 'pattern' && <div className='alert alert-danger'>Ingrese un correo valido</div>}
                                    <br />
                                    <input type="password" id="clave" className="fadeIn third" name="clave" placeholder="Contraseña" {...register('clave', { required: true })} />
                                    {errors.clave && errors.clave.type === 'required' && <div className='alert alert-danger'>Ingrese una clave</div>}
                                    <br />
                                    <button type="submit" className="btn btn-primary btn-lg" style={{background: '#162b4e'}}>Iniciar Sesión </button>
                                    
                                    <br />
                                    <br />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <Footer />
                <ul class="box-area">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>



            </div>
        </div>
    );
}

export default Login;