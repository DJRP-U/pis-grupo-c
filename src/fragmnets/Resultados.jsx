import Header from "./Header";
import Footer from "./Footer";
import '../css/style.css';
import DataTable from "react-data-table-component";
import { Button, Modal } from 'react-bootstrap';
import { useState } from "react";
import { listarEntregables } from "../hooks/Conexion";
import { getToken } from "../utilidades/Sessionutil";

const Resultados = () => {

    const [showForm, setShowForm] = useState(false);
    const [selectedRow, setSelectedRow] = useState({});
    const handleClose = () => setShowForm(false);

    const [llentregable, setLlentregable] = useState(false);
    const [entregables, setEntregables] = useState([]);
    const [searchTerm, setSearchTerm] = useState('');
    const [filteredData, setFilteredData] = useState(entregables); // data es tu array de datos original

    const handleSearch = () => {
        const filteredData = entregables.filter(
            item => item.nombre.toLowerCase().includes(searchTerm.toLowerCase())
        );
        console.log(filteredData)
        setFilteredData(filteredData);
    };

    if (!llentregable) {
        listarEntregables(getToken()).then((info) => {
            console.log(info);
            if (info.code === 200) {
                console.log(info.info);
                setEntregables(info.info);
            }
            setLlentregable(true);
        });
    }

    const columns = [
        {
            name: 'Nombre de la Practica',
            selector: row => row.practica.titulo,
            wrap: true
        },
        {
            name: 'Título entrega',
            selector: row => row.nombre,
            wrap: true
        },
        {
            name: 'Descripcion',
            selector: row => row.descripcion,
            wrap: true,
        },
        {
            name: 'Estudiante',
            selector: row => row.persona,
            wrap: true
        },
        {
            name: 'Facha de Entrega',
            selector: row => row.createdAt,
            wrap: true
        },
    ];

    const customStyles = {
        rows: {
            style: {
                minHeight: '72px', // override the row height
            },
        },
        headCells: {
            style: {
                paddingLeft: '8px', // override the cell padding for head cells
                paddingRight: '8px',
            },
        },
        cells: {
            style: {
                paddingLeft: '8px', // override the cell padding for data cells
                paddingRight: '8px',
                whiteSpace: 'normal', // permite el desbordamiento de texto
                overflow: 'visible', // permite que el contenido se desborde del contenedor
                textOverflow: 'unset', // desactiva el truncamiento del texto
            },
        },
    };

    const handleRowClick = (row) => {
        // Acción que deseas realizar al hacer clic en la fila
        console.log("Has hecho clic en una fila. La información de la fila es:", row);
        setSelectedRow(row);
        setShowForm(true);
    };

    return (
        <div className="wrapper">
            <Header />
            <div className="d-flex flex-column">
                <div className="content">

                    {/** DE AQUI CUERPO */}
                    <label>Aquí podra ver todas sus practicas finalizadas</label>
                    <div className='container-fluid'>
                        <input type="text" placeholder="que practica estas buscando" onChange={event => setSearchTerm(event.target.value)} />
                        <a className='btn btn-success' onClick={handleSearch} >Buscar</a>
                        <DataTable
                            columns={columns}
                            data={entregables}
                            responsive={true}
                            pagination={true}
                            highlightOnHover={true}
                            customStyles={customStyles}
                            onRowClicked={handleRowClick}
                        />
                        <div>
                        <label>Resultados de la Busqueda</label>
                        <DataTable
                            columns={columns}
                            data={filteredData}
                            responsive={true}
                            pagination={true}
                            highlightOnHover={true}
                            customStyles={customStyles}
                            onRowClicked={handleRowClick}
                        />
                        </div>
                        {showForm && (
                            <div className="model_box">
                                <Modal
                                    show={showForm}
                                    onHide={handleClose}
                                    backdrop="static"
                                    keyboard={false}
                                >
                                    <Modal.Header closeButton>
                                        <Modal.Title>Resumen de Resultados</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body>
                                        <div>
                                            <label>Resultados</label>
                                        </div>
                                        <div>
                                            <label>{selectedRow.descripcion}</label>
                                        </div>
                                        <div>
                                            <label>Contenido</label>
                                        </div>
                                        <div>
                                            <label>{selectedRow.trabajo}</label>
                                        </div>
                                        <div>
                                            <label>Nota</label>
                                        </div>
                                        <div>
                                            <label>1000</label>
                                        </div>

                                    </Modal.Body>

                                    <Modal.Footer>
                                        <Button variant="secondary" onClick={handleClose}>
                                            Cerrar
                                        </Button>

                                    </Modal.Footer>
                                </Modal>
                            </div>
                        )}
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
}

export default Resultados;