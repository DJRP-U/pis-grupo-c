import "bootstrap/dist/css/bootstrap.min.css";
import { Materias, PersonasSinMatricula } from "../hooks/Conexion";
import { Button, Modal, Input } from 'react-bootstrap';
import RegistrarPersona from "./RegistrarPersona";
import EditarPersona from "./EditarPersona";
import DataTable from "react-data-table-component";
import React, { useEffect, useState } from 'react';
import { Personas } from "../hooks/Conexion";
import { borrarSesion, getToken } from "../utilidades/Sessionutil";
import mensajes from "../utilidades/Mensajes";
import { useNavigate } from "react-router";
import Footer from "./Footer";
import Header from "./Header";

export const PresentarPersona = () => {
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const [data, setData] = useState([]);
    const [selectedRow, setSelectedRow] = useState({});
    const navegation = useNavigate();
    const [llPersonas, setLlPersonas] = useState(false);//para listar Personas
    const actualizar = () => setLlPersonas(false);//para listar Personas
    const [llVendidos, setLlVendidos] = useState(false);//para listar vendidos
    const vendidos = () => setLlVendidos(true);//para listar vendidos
    const [llDisponibles, setLlDisponibles] = useState(false);//para listar disponibles
    const disponibles = () => setLlDisponibles(true);//para listar disponibles
    const [selectedId, setSelectedId] = useState(null);//PARA SACAR EL ID DE LA TABLA
    const [show2, setShow2] = useState(false);//Model Box2
    
    const handleeClose = () => setShow2(false);//Model Box2
    const handleeShow = () => setShow2(true);//Model Box2
    
    const handleEditarPersona = async (row) => {
        setSelectedId(row.external_id);//Guarda el id en la variable selectedId
        setSelectedRow(row);
        localStorage.setItem('selectedId',row);
        console.log(localStorage.getItem('selectedId'));
        handleeShow();//Llama a el model de editar
    };//PARA SACAR EL ID DE LA TABLA

    const columns = [

        {
            name: 'nombres',
            selector: row => row.nombres,
        },
        {
            name: 'apellidos',
            selector: row => row.apellidos,
        },
        {
            name: 'identificacion',
            selector: row => row.identificacion,
        },
        {
            name: 'Direccion',
            selector: row => row.direccion,
        }
    ];
    if (!llPersonas) {
        PersonasSinMatricula(getToken()).then((info) => {
            if (info.error === true || info.msg === 'Token no valido o expirado!') {
                borrarSesion();
                mensajes(info.mensajes);
                navegation("/sesion");
            } else {
                console.log(info);
                setData(info.info);
            }
        })
        setLlPersonas(true);
    }

    return (

    <div>
        <Header/>
        <br/>
        <br/>
        <br/>
        <br/>
        <div className="container">
            
            
            
            <div className="crud shadow-lg p-3 mb-5 mt-5 bg-body rounded">
                <div className="row ">
                    <div  style={{ color: "#18122B" }}>
                        <h2><b>Gestionar Matriculas</b></h2>
                        </div>
                </div>
                <div className="row">
                <DataTable
                        columns={[
                            ...columns,
                            {
                                name: 'Acciones',
                                cell: (row) => (
                                    <div>
                                        {/* Botón para editar persona */}
                                        <Button variant="primary" onClick={() => handleEditarPersona(row)}>Matricular</Button>
                                    </div>
                                ),
                            },
                        ]}
                        data={data}
                    />

                </div>
                
                {/* <!--- Model Box ---> */}
                <div className="model_box">
                    <Modal
                        show={show}
                        onHide={handleClose}
                        backdrop="static"
                        keyboard={false}
                    >
                        <Modal.Header closeButton>
                            <Modal.Title>Agregar auto</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <RegistrarPersona/>
                        </Modal.Body>
                        

                        <Modal.Footer>
                            <Button variant="secondary" onClick={handleClose}>
                                Cerrar
                            </Button>

                        </Modal.Footer>
                    </Modal>



                </div>

                {/* <!--- Model Box2 ---> */}
                <div className="model_box">
                    <Modal
                        show={show2}
                        onHide={handleeClose}
                        backdrop="static"
                        keyboard={false}
                    >
                        <Modal.Header closeButton>
                            <Modal.Title>Asignar Matricula</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                        <EditarPersona row={selectedRow}/>
                        </Modal.Body>

                        <Modal.Footer>
                            <Button variant="secondary" onClick={handleeClose}>
                                Cerrar
                            </Button>

                        </Modal.Footer>
                    </Modal>
                </div>
            </div>
        
        </div>
    <Footer/>
    </div>
    );
}


export default PresentarPersona;