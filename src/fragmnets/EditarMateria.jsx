import { useForm } from "react-hook-form";
import { ModificarMateria, ObtenerMateria } from "../hooks/Conexion";
import mensajes from "../utilidades/Mensajes";
import { getToken } from "../utilidades/Sessionutil";
import { useState } from "react";

const EditarMateria = ({ row, onCloseModal }) => {
    const { register, handleSubmit, formState: { errors } } = useForm();
    const [llasignar, setLlAsignar] = useState(false);
    const [dato, setDato] = useState({});
    
    if (!llasignar) {
        ObtenerMateria(row.external_id, getToken()).then((info) => {
            Asignar(info.info);
            setLlAsignar(true);
        });
    }
    const onSubmit = (data) => {
        var datos = {
            "external_materia": row.external_id,
            "nombre": data.nombre
        };

        ModificarMateria(datos, getToken()).then((info) => {
            if (info.code != 200) {
                mensajes(info.msg, 'error', 'Error');
                //msgError(info.message);            
            } else {
                mensajes(info.msg);
                onCloseModal();
            }
        }
        );
    };
    function Asignar (dato) {
        console.log(dato);
        document.getElementById('nombre').value = dato.nombre;
    }
    return (
        <div className="p-6">
            <div>
                <form className="user" onSubmit={handleSubmit(onSubmit)}>
                    <input type="text" id="nombre" className="fadeIn second" style={{ width: '100%' }} placeholder="Nombre de la materia" {...register('nombre',{required:true, minLength: 4})}/>
                    {errors.nombre && errors.nombre.type === 'required' && <div className='alert alert-danger'>Ingrese un nuevo dato</div>}
                    {errors.nombre && errors.nombre.type === 'minLength' && (<div className='alert alert-danger'>El nombre debe tener al menos 4 caracteres</div>)}
                    <br />
                    <button type="submit" className="btn btn-primary btn-lg" > Modificar </button>
                </form>
            </div>
        </div>
    );
}

export default EditarMateria;