import '../css/style.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useState } from 'react';
import { ObtenerPersona } from '../hooks/Conexion';
import { getRol, getToken, getValor } from '../utilidades/Sessionutil';
import Header from './Header';
const Perfil = () => {
    const [nombres, setNombres] = useState('');
    const [apellidos, setApellidos] = useState('');
    const [direccion, setDireccion] = useState('');
    const [identificacion, setIdentificacion] = useState('');
    const [usuario, setUsuario] = useState('');
    const fecha = new Date(Date.now());
    ObtenerPersona(getValor('external')).then((info) => {

        if (info.code !== 200) {

        } else {
            setNombres(info.info.nombres);
            setApellidos(info.info.apellidos);
            setDireccion(info.info.direccion);
            setIdentificacion(info.info.identificacion);
            setUsuario(info.info.cuenta.correo);
        }
    })

    return (

        <div class="animation-area  mt-4 mb-4 p-3 d-flex justify-content-center ">
            <Header />
            <div class="card p-5" style={{height:'620px', width:'600px', margin: '4%'}}>
                <div class=" image d-flex flex-column justify-content-center align-items-center">
                    <button class="btn btn-secondary">
                        <img src="https://www.puertopenasco.tecnm.mx/wp-content/uploads/2022/01/perfil.png" height="100" width="100" />
                    </button>
                    <br></br>
                    <br></br>
                    <br></br>
                    <span class="name mt-3">Nombre: {nombres + ' ' + apellidos}</span>
                    <span class="idd">Correo: {usuario}</span>
                    <div class="d-flex flex-row justify-content-center align-items-center mt-3">
                        <span>Rol: {atob(getRol()).toUpperCase()}</span>
                    </div>
                    <div class="text mt-3">
                        <span>Dirección: {direccion}</span>
                    </div>
                    <div class="gap-3 mt-3 icons d-flex flex-row justify-content-center align-items-center"> <span>
                        <i class="fa fa-twitter">Identificación: {identificacion}</i></span> <span><i class="fa fa-facebook-f"></i>
                        </span> <span><i class="fa fa-instagram"></i>
                        </span> <span><i class="fa fa-linkedin"></i></span>
                    </div> <div class=" px-2 rounded mt-4 date ">
                        <span class="join">Fecha de acceso: {fecha.toDateString()}</span>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <div align="center" className="botonRegresar"><a href="/principal" ><button className="btn btn-secondary">Regresar</button></a></div>
               
                    </div>
                    
                     </div>
                <ul class="box-area">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
            </div>

        </div>

    )
}

export default Perfil;