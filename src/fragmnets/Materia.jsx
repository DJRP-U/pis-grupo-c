import Header from "./Header";
import Footer from "./Footer";
import '../css/style.css';
import '../css/MyDataTable.css'
import DataTable, { createTheme } from "react-data-table-component";
import { Button, Modal } from 'react-bootstrap';
import { useEffect, useState } from "react";
import { Materias } from "../hooks/Conexion";
import { getToken } from "../utilidades/Sessionutil";
import RegistroMateria from "./RegistroMateria";
import EditarMateria from "./EditarMateria";

const Resultados = () => {

    const [showForm, setShowForm] = useState(false);
    const [showForm2, setShowForm2] = useState(false);
    const [selectedRow, setSelectedRow] = useState({});
    const handleClose = () => { setShowForm(false); setShowForm2(false); setLlmaterias(false) };
    const openform = () => setShowForm(true);

    const [llmateria, setLlmaterias] = useState(false);
    const [materias, setMaterias] = useState([]);
    const [searchTerm, setSearchTerm] = useState('');
    const [filteredData, setFilteredData] = useState(materias); // data es tu array de datos original

    const handleSearch = () => {
        const filteredData = materias.filter(item => {
            const nombreMatches = item.nombre.toLowerCase().includes(searchTerm.toLowerCase());
            const externalIdMatches = item.external_id.includes(searchTerm);

            return nombreMatches || externalIdMatches;
        });
        setFilteredData(filteredData);
    };

    useEffect(() => {
        handleSearch();
    }, [searchTerm]);

    if (!llmateria) {
        Materias(getToken()).then((info) => {
            console.log(info);
            if (info.code === 200) {
                console.log(info.info);
                setMaterias(info.info);
                setFilteredData(info.info);
            }
            setLlmaterias(true);
        });
    }

    const columns = [
        {
            name: 'Materia',
            selector: row => row.nombre,
            wrap: true
        },
        {
            name: 'Código de Materia',
            selector: row => row.external_id,
            wrap: true
        }
    ];

    const customStyles = {
        rows: {
            style: {
                minHeight: '72px', // override the row height
                fontFamily: 'Arial, sans-serif',
            },
        },
        headCells: {
            style: {
                paddingLeft: '8px', // override the cell padding for head cells
                paddingRight: '8px',
                fontSize: '18px',
                fontFamily: 'Arial, sans-serif',
            },
        },
        cells: {
            style: {
                paddingLeft: '8px', // override the cell padding for data cells
                paddingRight: '8px',
                fontSize: '16px',
            },
        },
    };

    const handleRowClick = (row) => {
        // Acción que deseas realizar al hacer clic en la fila
        console.log("Has hecho clic en una fila. La información de la fila es:", row);
        setSelectedRow(row);
        setShowForm2(true);
    };

    return (
        <div className="wrapper">
            <Header />
            <div className="d-flex flex-column">
                <div className="content">

                    {/** DE AQUI CUERPO */}
                    <label>Materias</label>
                    <div className='container-fluid'>
                        <input
                            type="text"
                            placeholder="Qué materia estás buscando"
                            value={searchTerm}
                            onChange={event => setSearchTerm(event.target.value)}
                        />
                        <br />
                        <div class="button-container">
                            <Button onClick={openform}>Agregar Materia +</Button>
                        </div>
                        <br />
                        <DataTable
                            className="my-data-table"
                            striped
                            fixedHeader
                            columns={columns}
                            data={filteredData}
                            responsive={true}
                            highlightOnHover={true}
                            onRowDoubleClicked={handleRowClick}
                        />
                        {showForm && (
                            <div className="model_box">
                                <Modal
                                    show={showForm}
                                    onHide={handleClose}
                                    backdrop="static"
                                    keyboard={false}
                                >
                                    <Modal.Header closeButton>
                                        <Modal.Title>Agregar Nueva Materia</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body>
                                        <RegistroMateria onCloseModal={handleClose} />
                                    </Modal.Body>

                                    <Modal.Footer>
                                        <Button variant="secondary" onClick={handleClose}>
                                            Cancelar
                                        </Button>

                                    </Modal.Footer>
                                </Modal>
                            </div>
                        )}

                        {showForm2 && (
                            <div className="model_box">
                                <Modal
                                    show={showForm2}
                                    onHide={handleClose}
                                    backdrop="static"
                                    keyboard={false}
                                >
                                    <Modal.Header closeButton>
                                        <Modal.Title>Editar Materia</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body>
                                        <EditarMateria row={selectedRow} onCloseModal={handleClose} />
                                    </Modal.Body>

                                    <Modal.Footer>
                                        <Button variant="secondary" onClick={handleClose}>
                                            Cerrar
                                        </Button>

                                    </Modal.Footer>
                                </Modal>
                            </div>
                        )}
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
}

export default Resultados;