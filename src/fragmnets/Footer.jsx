import "bootstrap/dist/css/bootstrap.min.css";


const Footer = () => {
    return (
        <div className="contenedor_completo">
            <div className="wrapper fadeInDown">
                <div id="formContent">
                    <div className="fadeIn first">
                        <footer className="py-3 my-4">
                            <ul className="nav justify-content-center border-bottom pb-3 mb-3">
                                <li className="nav-item"><a href="#" className="nav-link px-2 text-muted">Inicio</a></li>
                                <li className="nav-item"><a href="#" className="nav-link px-2 text-muted">Practicas</a></li>
                                <li className="nav-item"><a href="#" className="nav-link px-2 text-muted">Preguntas frecuentes</a></li>
                                <li className="nav-item"><a href="#" className="nav-link px-2 text-muted">Sobre nosotros</a></li>
                            </ul>
                            <p className="text-center text-muted">© 2023 Laboratorio Remoto</p>
                        </footer>
                    </div>
                </div>
            </div>
        </div>

    );
}

export default Footer;