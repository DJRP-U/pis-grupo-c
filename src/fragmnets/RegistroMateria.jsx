import { useForm } from "react-hook-form";
import { GuardarMaterias } from "../hooks/Conexion";
import mensajes from "../utilidades/Mensajes";
import { getToken } from "../utilidades/Sessionutil";

const RegistroMateria = ({onCloseModal}) => {
    const { register, handleSubmit, formState: { errors } } = useForm();
    const onSubmit = (data) => {
        var datos = {
          "nombre": data.nombre, 
        };
        GuardarMaterias(datos, getToken()).then((info) => {          
          if(info.code != 200){
            //console.log(info);
            mensajes(info.msg, 'error', 'Error');
            //msgError(info.message);            
          } else {            
            mensajes(info.msg);
            onCloseModal();
          }
        }
        );
    };
    return (
        <div className="p-6">
            <div>
                <form className="user" onSubmit={handleSubmit(onSubmit)}>
                    <input type="text" id="nombres" className="fadeIn second" style={{ width: '100%' }} placeholder="Nombre de la materia" {...register('nombre',{required:true, minLength: 4})}/>
                    {errors.nombre && errors.nombre.type === 'required' && <div className='alert alert-danger'>Ingrese el Nombre de la materia</div>}
                    {errors.nombre && errors.nombre.type === 'minLength' && (<div className='alert alert-danger'>El nombre debe tener al menos 4 caracteres</div>)}
                    <br />
                    <button type="submit" className="btn btn-primary btn-lg" > Agregar </button>
                </form>
            </div>
        </div>
    );
}

export default RegistroMateria;