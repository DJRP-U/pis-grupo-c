import '../css/style.css';
import { borrarSesion, getRol, getToken, getValor } from '../utilidades/Sessionutil';
import { useNavigate } from 'react-router';
import { useForm } from 'react-hook-form';
import { useState } from 'react';
import { MateriasCursa } from '../hooks/Conexion';
import { getMateria, saveMateria } from '../utilidades/ides';

const Header = () => {
  const navegation = useNavigate();
  const [materias, setMaterias] = useState([]);
  const [llmateria, setLlmaterias] = useState(false);

  const handleClick = () => {
    borrarSesion();
    navegation('/login');
  }

  const botonRegistrar = () => {
    const auxRol = atob(getRol());
    if ("admin" === auxRol) {
      return(<li style={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'center' }} >
        <a className="nav-link scrollto" href="/registrar">Registrar</a></li>
        )
    }
  }
  const botonMatricular = () => {
    const auxRol = atob(getRol());
    if ("admin" === auxRol) {
      return(<li style={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'center' }} >
        <a className="nav-link scrollto" href="/matricular">Matricular</a></li>
        )
    }
  }

  const botonMateria = () => {
    const auxRol = atob(getRol());
    if ("admin" === auxRol) {
      return(<li style={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'center' }} >
        <a className="nav-link scrollto" href="/materia">Materias</a></li>
        )
    }
  }

  const perfil = () =>{
    const auxNombre =  getValor('nombre')
    return (<li style={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'center' }}>
      <img src="https://www.puertopenasco.tecnm.mx/wp-content/uploads/2022/01/perfil.png" width="50" height="50" style={{ marginLeft: '50px' }}/><a className="nav-link scrollto" href="/perfil">{auxNombre}</a></li>)
  }



  if (!llmateria) {
    MateriasCursa(getValor('external'), getToken()).then((info) => {
      console.log(info);
      if (info.code === 200) {
        console.log(info.info);
        setMaterias(info.info);
      }
      setLlmaterias(true);
    });
  }

  const isSelect = (event) => {
    const selectedValue = event.target.value;
    if (selectedValue != "default") {
      saveMateria(selectedValue);
      //if(atob(getRol())=="profesor"){
        //navegation('/practicadoc')
      //} else if(atob(getRol())=="estudiante"){
        //navegation('/practica/estudiante')
      //}
      navegation('/sidebar');
      window.location.reload();
    }
  };

  return (<header id="header" className="fixed-top header-inner-pages">
    <div className="d-flex align-items-center justify-content-between">

      <h1 className="logo"><a href="/principal">Laboratorio remoto</a></h1>

      <nav id="navbar" className="navbar">
        <ul>
          <li><a className="nav-link scrollto" href="/principal">Inicio</a></li>
          {botonRegistrar()}
          {botonMateria()}
          {botonMatricular()}
          <li><a className="nav-link scrollto" onClick={handleClick}>Salir</a></li>
          {perfil()}
          <li>
            <select className="select-materias" onClick={isSelect}>
              <option value="default" disabled selected>Seleccionar Materia</option>
              {materias.map((aux, index) => (
                <option key={index} value={aux.external_id}>{aux.nombre}</option>
              ))}
            </select>
          </li>
        </ul>
        
        <i className="bi bi-list mobile-nav-toggle"></i>
      </nav>

    </div>
  </header>)
}

export default Header;